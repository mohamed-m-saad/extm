#include "libitm_i.h"

namespace GTM HIDDEN {
  /**
   *  Specializations for adding each of libITM's 10 primitive types to the
   *  write log.
   */
  INSTANTIATE_BST(uint8_t, 8);
  INSTANTIATE_BST(uint16_t, 16);
  INSTANTIATE_BST(uint32_t, 24);
  INSTANTIATE_BST(uint64_t, 32);
  INSTANTIATE_BST(float, 40);
  INSTANTIATE_BST(double, 48);
  INSTANTIATE_BST(long double, 56);
  INSTANTIATE_UNSUPPORTED(float _Complex);
  INSTANTIATE_UNSUPPORTED(double _Complex);
  INSTANTIATE_UNSUPPORTED(long double _Complex);

} // namespace GTM
