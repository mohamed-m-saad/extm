/* Copyright (C) 2012-2015 Free Software Foundation, Inc.
   Contributed by Torvald Riegel <triegel@redhat.com>.

   This file is part of the GNU Transactional Memory Library (libitm).

   Libitm is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   Libitm is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
   FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
   more details.

   Under Section 7 of GPL version 3, you are granted additional
   permissions described in the GCC Runtime Library Exception, version
   3.1, as published by the Free Software Foundation.

   You should have received a copy of the GNU General Public License and
   a copy of the GCC Runtime Library Exception along with this program;
   see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see
   <http://www.gnu.org/licenses/>.  */

#include "libitm_i.h"
#include <stdio.h>

using namespace GTM;

namespace {

// This group consists of all TM methods that detect conflicts via
// value-based validation
struct snorec_mg : public method_group
{
  // Maximum time is all bits except the lock bit and the overflow reserve bit
  static const gtm_word TIME_MAX = (~(gtm_word)0 >> 2);

  // The shared time base.
  atomic<gtm_word> time __attribute__((aligned(HW_CACHELINE_SIZE)));

  virtual void init()
  {
    // This store is only executed while holding the serial lock, so relaxed
    // memory order is sufficient here.
    time.store(0, memory_order_relaxed);
  }

  virtual void fini() { }

  // We only re-initialize when our time base overflows.  Thus, only reset
  // the time base and the orecs but do not re-allocate the orec array.
  virtual void reinit()
  {
    // This store is only executed while holding the serial lock, so relaxed
    // memory order is sufficient here.
    time.store(0, memory_order_relaxed);
  }
};

static snorec_mg o_snorec_mg;

// The NOrec TM method.
// Uses a single global lock to protect writeback, and uses values for
// validation
//
// Note that NOrec does not require quiescence-based validation, though we
// employ it to simplify memory reclamation and remain consistent with the
// rest of the GCC-TM implementations
class snorec_dispatch : public abi_dispatch
{
protected:

  // Returns true iff all locations read by the transaction still have the
  // values observed by the transaction
  static gtm_word validate(gtm_thread *tx)
  {
    while (true) {
      // read the lock until it is even
      gtm_word s = o_snorec_mg.time.load(memory_order_acquire);
      if ((s & 1) == 1)
        continue;

      // check the read set... the read set is technically an "undo log", but
      // that's not important to this code
      bool valid = tx->valuelog.valuecmp();

      if (!valid)
          // does not allow -1, gtm_word is unsigned int
          return -1;

      // make sure lock didn't change during validation
      tx->shared_state.store(s, memory_order_release);
      if (o_snorec_mg.time.load(memory_order_acquire) == s)
          return s;
    }
  }

  template <typename V> static int rmu(V* addr, const V delta, int op, ls_modifier mod)
  {
//	  printf("RMU-start\n");

	  // filter out writes to the stack frame
	  gtm_thread *tx = gtm_thr();
	  void *top = mask_stack_top(tx);
	  void *bot = mask_stack_bottom(tx);
	  if ((addr <= top && (uint8_t*) addr > bot ) ||
		 ((uint8_t*)addr < bot && ((uint8_t*)addr + sizeof(V) > bot))) {
		switch(op){
			case OP_INC:	*addr += delta; break;
			case OP_DEC:	*addr -= delta; break;
			case OP_MUL:	*addr *= delta; break;
			case OP_DIV:	*addr /= delta; break;
			default:
//				printf("RMU-invalid\n");
				return -1;
		}
	    return 1;
	  }
	  int code;
	  switch(op){
	  	  case OP_INC: code = 2; break;
	  	  case OP_DEC: code = 3; break;
	  	  case OP_MUL: code = 4; break;
	  	  case OP_DIV: code = 5; break;
	  	  default  :   code = 1; break;
	  }
	  // insert into the log, so we can calculate it back later
	  tx->redolog_bst.insert((V*)addr, delta, code);
//	  printf("RMU-end\n");
	  return 1;
  }

  static inline int inverse_opcode(int opcode)
  {
	  switch(opcode){
	  	case OP_EQ:	  return OP_NE;
	  	case OP_NE:	  return OP_EQ;
	  	case OP_GT:	  return OP_LE;
	  	case OP_GE:	  return OP_LT;
	  	case OP_LT:	  return OP_GE;
	  	case OP_LE:   return OP_GT;
	  	default:	  return -1;
	  }
  }

#define VALIDATE_AND_READ(tx, addr, v)                                           \
	  /* NOrec read loop: */                                                     \
	  /* A read is valid iff it occurs during a period where the seqlock does*/  \
	  /* not change and is even.  This code also polls for new changes that  */  \
	  /* might necessitate a validation. */                                      \
	  v = *addr;                                                                 \
	  /* get start time, compared to the current timestamp */                    \
	  gtm_word start_time = tx->shared_state.load(memory_order_acquire);         \
	  while (start_time != o_snorec_mg.time.load(memory_order_acquire)) {        \
		  if ((start_time = validate(tx)) == (gtm_word)-1) {                     \
			  tx->restart_reason[RESTART_VALIDATE_READ]++;                       \
			  tx->restart(RESTART_VALIDATE_READ);                                \
		  }/*Abort*/                                                             \
		  v = *addr;                                                             \
	  }                                                                          \

  template <typename V> static bool compare(const V* addr, const V val, int op, ls_modifier mod)
  {
//	  printf("Compare-start %p\n", (void*)addr);
      gtm_thread *tx = gtm_thr();

      // stack filter: if we are reading from a stack location, just read it
      // without logging or write set lookup
      void *top = mask_stack_top(tx);
      void *bot = mask_stack_bottom(tx);
      if ((addr <= top && (uint8_t*) addr > bot ) ||
          ((uint8_t*)addr < bot && ((uint8_t*)addr + sizeof(V) > bot))) {
//    	  printf("Compare-end1\n");
          return cmp(*addr, val, op);
      }

      // check the redo log
      V v;
      bool need_promot; // read after semantic write
      if (!tx->redolog_bst.isEmpty() && tx->redolog_bst.find(addr, v, need_promot) != 0){
    	  if(need_promot){
//    		  printf("We need promot here!\n");
    	      VALIDATE_AND_READ(tx, addr, v)
			  /* log address and value into read log */
			  tx->valuelog.log_cmp(addr, sizeof(V), &v, OP_EQ);
    		  v = tx->redolog_bst.promot(addr, v);
    	  }
//    	  printf("Compare-end2\n");
          return cmp(v, val, op);
      }

      VALIDATE_AND_READ(tx, addr, v)
      bool result = cmp(v, val, op);
      // log address, opcode (or its inverse), and compared value into compare log
      if(result)
    	  tx->valuelog.log_cmp(addr, sizeof(V), &val, op);
      else
    	  tx->valuelog.log_cmp(addr, sizeof(V), &val, inverse_opcode(op));
//      printf("Compare-end3\n");
      return result;
  }

  template <typename V> static inline bool cmp(const V val1, const V val2, const int op)
  {
	  switch(op){
	  	case OP_EQ:	  return val1 == val2;
	  	case OP_NE:   return val1 != val2;
	  	case OP_GT:	  return val1 > val2;
	  	case OP_GE:   return val1 >= val2;
	  	case OP_LT:	  return val1 < val2;
	  	case OP_LE:   return val1 <= val2;
	  	default:	  return false; // not possible
	  }
  }

  template <typename V> static int compare(const V* addr1, const V* addr2, int op, ls_modifier mod)
  {
	  V val2 = load(addr2, mod);
	  return compare(addr1, val2, op, mod);
  }

  template <typename V> static V load(const V* addr, ls_modifier mod)
  {
//	  printf("Load %p\n", (void*)addr);
      gtm_thread *tx = gtm_thr();

      // stack filter: if we are reading from a stack location, just read it
      // without logging or write set lookup
      void *top = mask_stack_top(tx);
      void *bot = mask_stack_bottom(tx);
      if ((addr <= top && (uint8_t*) addr > bot ) ||
          ((uint8_t*)addr < bot && ((uint8_t*)addr + sizeof(V) > bot))) {
          return *addr;
      }

      // check the redo log
      V v;
      bool need_promot; // read after semantic write
      if (!tx->redolog_bst.isEmpty() && tx->redolog_bst.find(addr, v, need_promot) != 0){
    	  if(need_promot){
//    		  printf("We need promot here!\n");
    	      VALIDATE_AND_READ(tx, addr, v)
			  /* log address and value into read log */
			  tx->valuelog.log_cmp(addr, sizeof(V), &v, OP_EQ);
    		  return tx->redolog_bst.promot(addr, v);
    	  }
          return v;
      }

      VALIDATE_AND_READ(tx, addr, v)
	  /* log address and value into read log */
	  tx->valuelog.log_cmp(addr, sizeof(V), &v, OP_EQ);
      return v;
  }

  template <typename V> static void store(V* addr, const V value,
      ls_modifier mod)
  {
//	printf("Store %p\n", (void*)addr);
    // filter out writes to the stack frame
    gtm_thread *tx = gtm_thr();
    void *top = mask_stack_top(tx);
    void *bot = mask_stack_bottom(tx);
    if ((addr <= top && (uint8_t*) addr > bot ) ||
        ((uint8_t*)addr < bot && ((uint8_t*)addr + sizeof(V) > bot))) {
      *addr = value;
      return;
    }

    // insert into the log, so we can write it back later
    tx->redolog_bst.insert(addr, value, 1);
  }

public:
  static void memtransfer_static(void *dst, const void* src, size_t size,
      bool may_overlap, ls_modifier dst_mod, ls_modifier src_mod)
  {
    // [transmem] This code is far from optimal

    // [transmem] we copy byte-by-byte, since we need to do log lookups.
    // That means we can treat the type as unsigned chars.
    //
    // Note that we can ignore overlap, since we do buffered writes
    unsigned char *srcaddr = (unsigned char*)const_cast<void*>(src);
    unsigned char *dstaddr = (unsigned char*)dst;

    for (size_t i = 0; i < size; i++) {
      unsigned char temp = load<unsigned char>(srcaddr, RaR);
      store<unsigned char>(dstaddr, temp, WaW);
      dstaddr = (unsigned char*) ((long long)dstaddr + sizeof(unsigned char));
      srcaddr = (unsigned char*) ((long long)srcaddr + sizeof(unsigned char));
    }
  }

  static void memset_static(void *dst, int c, size_t size, ls_modifier mod)
  {
    unsigned char* dstaddr = (unsigned char*)dst;

    // [transmem] save data into redo log... note that the modifier doesn't
    //            matter
    for (size_t it = 0; it < size; it++) {
      store<unsigned char>(dstaddr, (unsigned char)c, WaW);
      dstaddr = (unsigned char*) ((long long)dst + sizeof(unsigned char));
    }
  }

  virtual gtm_restart_reason begin_or_restart()
  {
//	printf("Start\n");
    // NB: We don't need to do anything for nested transactions.
    gtm_thread *tx = gtm_thr();

    // Read the current time, which becomes our snapshot time.
    // Use acquire memory oder so that we see the lock acquisitions by update
    // transcations that incremented the global time (see trycommit()).
    gtm_word snapshot = o_snorec_mg.time.load(memory_order_acquire);
    // Sample the sequence lock, if it is even decrement by 1
    snapshot = snapshot & ~(1L);

    // Re-initialize method group on time overflow.
    if (snapshot >= o_snorec_mg.TIME_MAX)
      return RESTART_INIT_METHOD_GROUP;

    // We don't need to enforce any ordering for the following store. There
    // are no earlier data loads in this transaction, so the store cannot
    // become visible before those (which could lead to the violation of
    // privatization safety). The store can become visible after later loads
    // but this does not matter because the previous value will have been
    // smaller or equal (the serial lock will set shared_state to zero when
    // marking the transaction as active, and restarts enforce immediate
    // visibility of a smaller or equal value with a barrier (see
    // rollback()).
    tx->shared_state.store(snapshot, memory_order_relaxed);
    return NO_RESTART;
  }

  virtual bool trycommit(gtm_word& priv_time)
  {
//	printf("Try-Commit\n");
    gtm_thread* tx = gtm_thr();
    gtm_word start_time = 0;

    // If we haven't updated anything, we can commit. Just clean value log.
    if (tx->redolog_bst.isEmpty()) {
      tx->valuelog.commit();
//      printf("Commit1\n");
      return true;
    }

    // get start time
    start_time = tx->shared_state.load(memory_order_relaxed);

    // get the lock and validate
    // compare_exchange_weak should save some overhead in a loop?
    while (!o_snorec_mg.time.compare_exchange_weak
           (start_time, start_time + 1, memory_order_acquire)) {
      if ((start_time = validate(tx)) == (gtm_word)-1) {
        tx->restart_reason[RESTART_VALIDATE_READ]++;
//        printf("Commit-Fail\n");
        return false;
      }
    }

    // do write back
    tx->redolog_bst.writeback();

    // relaese the sequence lock
    gtm_word ct = start_time + 2;
    o_snorec_mg.time.store(ct, memory_order_release);

    // We're done, clear the logs.
    tx->redolog_bst.reset();
    // NB: this clears the log, although it is called "commit"
    tx->valuelog.commit();

    // Need to ensure privatization safety. Every other transaction must
    // have a snapshot time that is at least as high as our commit time
    // (i.e., our commit must be visible to them).
    priv_time = ct;
//    printf("Commit2\n");
    return true;
  }

  virtual void rollback(GTM::gtm_transaction_cp* cp)
  {
//	printf("Rollback\n");
    // We don't do anything for rollbacks of nested transactions.
    if (cp != 0)
      return;
      
    gtm_thread *tx = gtm_thr();

    // We need this release fence to ensure that privatizers see the
    // rolled-back original state (not any uncommitted values) when they read
    // the new snapshot time that we write in begin_or_restart().
    atomic_thread_fence(memory_order_release);

    // We're done, clear the logs.
    tx->redolog_bst.reset();
    // NB: this clears the log, although it is called "commit"
    tx->valuelog.commit();
  }

  virtual bool supports(unsigned number_of_threads)
  {
    // NOrec can support any number of threads
    return true;
  }

  CREATE_DISPATCH_METHODS(virtual, )
  CREATE_DISPATCH_METHODS_MEM()

  snorec_dispatch() : abi_dispatch(false, true, false, false, 0, &o_snorec_mg)
  { }
};

} // anon namespace

static const snorec_dispatch o_snorec_dispatch;

abi_dispatch *
GTM::dispatch_snorec ()
{
  return const_cast<snorec_dispatch *>(&o_snorec_dispatch);
}
