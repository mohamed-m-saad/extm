################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/BankBench.cpp \
../src/HashtableBench.cpp \
../src/LRUBench.cpp \
../src/bmharness.cpp \
../src/rand32.cpp 

OBJS += \
./src/BankBench.o \
./src/HashtableBench.o \
./src/LRUBench.o \
./src/bmharness.o \
./src/rand32.o 

CPP_DEPS += \
./src/BankBench.d \
./src/HashtableBench.d \
./src/LRUBench.d \
./src/bmharness.d \
./src/rand32.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -O0 -g3 -Wall -fgnu-tm -fdump-tree-all-raw -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


