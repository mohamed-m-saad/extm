bench=$1
x=1000000
o=10
m=1000
for w in  2 4 6 8 10 12 14 16 18 20 22 24
do
  echo $w" Threads"
  export ITM_DEFAULT_METHOD=norec
  ./opt-sem     -X$x -O$o -m$m -p$w >> $bench-norecopt-$o.log
  export ITM_DEFAULT_METHOD=snorec
  ./noopt-nosem -X$x -O$o -m$m -p$w >> $bench-norec-$o.log
  ./opt-sem     -X$x -O$o -m$m -p$w >> $bench-snorec-$o.log
done
echo "Norec"
grep throughput $bench-norec-$o.log
echo "Norec-Optimized"
grep throughput $bench-norecopt-$o.log
echo "SNorec"
grep throughput $bench-snorec-$o.log
