reset

export tm_semantic=false
export tm_optimize=false
make clean;make
mv benchGCC noopt-nosem
rm -rf src-noopt-nosem
cp -r src src-noopt-nosem

export tm_semantic=true
export tm_optimize=false
make clean;make
mv benchGCC noopt-sem
rm -rf src-noopt-sem
cp -r src src-noopt-sem

export tm_semantic=true
export tm_optimize=true
make clean;make
mv benchGCC opt-sem
rm -rf src-opt-sem
cp -r src src-opt-sem

#./run.sh ml_wt
