#include <pthread.h>
#include <stdio.h>

int z, f;
/* this function is run by the thread */
void *inc_x(void *x_void_ptr) {
	/* increment x to 100 */
	int *x_ptr = (int *) x_void_ptr;
	int i;
	int t;
	for(i=0;i<10000;i++)
		__transaction_atomic {
			t = z > f;
			++(*x_ptr);
		}
	printf("x increment finished\n");

	/* the function must return something - NULL will do */
	return (void*) t;
}

int main() {
	int x = 0, y = 0;

	/* show the initial values of x and y */
	printf("x: %d, y: %d\n", x, y);

	/* this variable is our reference to the second thread */
	pthread_t inc_x_thread[10];
	int i;
	for (i = 0; i < 10; i++)
		/* create a thread which executes inc_x(&x) */
		if (pthread_create(&inc_x_thread[i], NULL, inc_x, &x)) {
			fprintf(stderr, "Error creating thread\n");
			return 1;
		}

	for (i = 0; i < 10; i++)
		/* wait for the thread to finish */
		if (pthread_join(inc_x_thread[i], NULL)) {
			fprintf(stderr, "Error joining thread\n");
			return 2;
		}

	/* show the results - x is now 100 thanks to the second thread */
	printf("x: %d, y: %d\n", x, y);

	return 0;

}
