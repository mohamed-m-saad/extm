#include <stdio.h>
struct account{
  int balance;
};
struct account *a;
void main(){
  a->balance = 10;
  __transaction_atomic { a->balance++; } 
  printf("Bye %d\n", a->balance);
}
