/*
 * def.hpp
 *
 *  Created on: Jan 30, 2016
 *      Author: Moahmed
 */

#ifndef INCLUDE_API_DEF_HPP_
#define INCLUDE_API_DEF_HPP_

// TM comparison op-codes
#define TM_EQ 	0
#define TM_IEQ 	1
#define TM_GT 	2
#define TM_GTE 	3
#define TM_LT 	4
#define TM_LTE 	5
#define TM_AND 	6
#define TM_OR 	7


#endif /* INCLUDE_API_DEF_HPP_ */
