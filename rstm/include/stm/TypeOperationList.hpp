/**
 *  Copyright (C) 2015
 *  Virginia Tech
 *
 * License: Modified BSD
 *          Please see the file LICENSE.RSTM for licensing information
 */

#ifndef STM_TYPE_OPERATION_LIST_HPP
#define STM_TYPE_OPERATION_LIST_HPP

/**
 *  We use the OperationList class to log address/value/operation pairs for our
 *  value-based-validation implementations---NOrec
 *
 *  This word-granularity continues to be correct when we have enabled byte
 *  logging (because we're building for C++ TM compatibility), but it introduces
 *  the possibility of byte-level false conflicts. One of VBV's advantages is
 *  that there are no false conflicts. In order to preserve this behavior, we
 *  offer the user the option to use the byte-mask (which is already enabled for
 *  byte logging) to do byte-granularity validation. The disadvantage to this
 *  technique is that the read log entry size is increased by the size of the
 *  stored mask (we could optimize for 64-bit Linux and pack the mask into an
 *  unused part of the logged address, but we don't yet have this capability).
 *
 *  This file implements the value log given the current configuration settings
 *  in stm/config.h
 */
#include "stm/config.h"
#include "stm/MiniVector.hpp"
#include <api/operator.hpp>

namespace stm {

  /**
   *  When we're word logging we simply store address/value pairs in the
   *  OperationList.
   */
  class WordLoggingTypeOperationListEntry {
    public:
      void* val;
      void** addr;
      WordLoggingTypeOperationListEntry(void** a, void* v) : addr(a), val(v) {
      }
  };

typedef WordLoggingTypeOperationListEntry TypeOperationListEntry;
#define STM_TYPE_OPERATION_LIST_ENTRY(addr, op, val, mask) TypeOperationListEntry(addr, val), op

  struct TypeOperationList {
	  MiniVector<TypeOperationListEntry> eq;
	  MiniVector<TypeOperationListEntry> ieq;
	  MiniVector<TypeOperationListEntry> gt;
	  MiniVector<TypeOperationListEntry> gte;
	  MiniVector<TypeOperationListEntry> lt;
	  MiniVector<TypeOperationListEntry> lte;
	  TypeOperationList(const unsigned long cap) : eq(cap), ieq(cap), gt(cap), gte(cap), lt(cap), lte(cap) {
      }

	  void insert(TypeOperationListEntry entry, int operation){
		  switch(operation){
			case TM_EQ:		eq.insert(entry); break;
			case TM_IEQ:	ieq.insert(entry); break;
			case TM_GT:		gt.insert(entry); break;
			case TM_GTE:	gte.insert(entry); break;
			case TM_LT:		lt.insert(entry); break;
			case TM_LTE:	lte.insert(entry); break;
		  }
	  }

	  bool isValid(){
		  bool valid = true;
          foreach (MiniVector<TypeOperationListEntry>, i, eq)
              valid &= (*i->addr == i->val);
          foreach (MiniVector<TypeOperationListEntry>, i, ieq)
              valid &= (*i->addr != i->val);
          foreach (MiniVector<TypeOperationListEntry>, i, gt)
              valid &= ((long)(*i->addr) > (long)i->val);
          foreach (MiniVector<TypeOperationListEntry>, i, gte)
              valid &= ((long)(*i->addr) >= (long)i->val);
          foreach (MiniVector<TypeOperationListEntry>, i, lt)
              valid &= ((long)(*i->addr) < (long)i->val);
          foreach (MiniVector<TypeOperationListEntry>, i, lte)
              valid &= ((long)(*i->addr) <= (long)i->val);
          return valid;
	  }

	  void reset(){
		  eq.reset();
		  ieq.reset();
		  gt.reset();
		  gte.reset();
		  lte.reset();
		  lt.reset();
	  }
  };
}

#endif // STM_TYPE_OPERATION_LIST_HPP
