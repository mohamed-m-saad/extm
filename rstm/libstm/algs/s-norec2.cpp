/**
 *  Copyright (C) 2015
 *  Virginia Tech
 *
 * License: Modified BSD
 *          Please see the file LICENSE.RSTM for licensing information
 */

/**
 *  NOrec Implementation
 *
 *    This STM was published by Dalessandro et al. at PPoPP 2010.  The
 *    algorithm uses a single sequence lock, along with value-based validation,
 *    for concurrency control.  This variant offers semantics at least as
 *    strong as Asymmetric Lock Atomicity (ALA).
 */

#include "../cm.hpp"
#include "algs.hpp"
#include "RedoRAWUtils.hpp"

// Don't just import everything from stm. This helps us find bugs.
using stm::TxThread;
using stm::timestamp;
using stm::WriteSetEntry;
using stm::TypeOperationList;
using stm::TypeOperationListEntry;


namespace {

  const uintptr_t VALIDATION_FAILED = 1;
  NOINLINE uintptr_t validate(TxThread*);
  bool irrevoc(STM_IRREVOC_SIG(,));
  void onSwitchTo();

  template <class CM>
  struct NOrec2_Semantic
  {
      static TM_FASTCALL bool begin(TxThread*);
      static TM_FASTCALL void commit(STM_COMMIT_SIG(,));
      static TM_FASTCALL void commit_ro(STM_COMMIT_SIG(,));
      static TM_FASTCALL void commit_rw(STM_COMMIT_SIG(,));
      static TM_FASTCALL void* read_ro(STM_READ_SIG(,,));
      static TM_FASTCALL void* read_rw(STM_READ_SIG(,,));
      static TM_FASTCALL void write_ro(STM_WRITE_SIG(,,,));
      static TM_FASTCALL void write_rw(STM_WRITE_SIG(,,,));
      static TM_FASTCALL bool compare(STM_COMPARE_SIG(,,,,));
      static TM_FASTCALL void increment_decimal(STM_INCREMENT_I_SIG(,,,));
      static TM_FASTCALL void increment_floating(STM_INCREMENT_F_SIG(,,,));
      static stm::scope_t* rollback(STM_ROLLBACK_SIG(,,,));
      static void initialize(int id, const char* name);
  };

  uintptr_t
  validate(TxThread* tx)
  {
#ifdef STATS
	  tx->tx_num_validates++;
#endif
      while (true) {
          // read the lock until it is even
          uintptr_t s = timestamp.val;
          if ((s & 1) == 1)
              continue;

          // check the read set
          CFENCE;
          // don't branch in the loop---consider it backoff if we fail
          // validation early
          if (!tx->tlist.isValid())
              return VALIDATION_FAILED;

          // restart if timestamp changed during read set iteration
          CFENCE;
          if (timestamp.val == s)
              return s;
      }
  }

  bool
  irrevoc(STM_IRREVOC_SIG(tx,upper_stack_bound))
  {
      while (!bcasptr(&timestamp.val, tx->start_time, tx->start_time + 1))
          if ((tx->start_time = validate(tx)) == VALIDATION_FAILED)
              return false;

      // redo writes
      tx->writes.writeback(STM_WHEN_PROTECT_STACK(upper_stack_bound));

      // Release the sequence lock, then clean up
      CFENCE;
      timestamp.val = tx->start_time + 2;
      tx->tlist.reset();
      tx->writes.reset();
      return true;
  }

  void
  onSwitchTo() {
      // We just need to be sure that the timestamp is not odd, or else we will
      // block.  For safety, increment the timestamp to make it even, in the event
      // that it is odd.
      if (timestamp.val & 1)
          ++timestamp.val;
  }


  template <typename CM>
  void
  NOrec2_Semantic<CM>::initialize(int id, const char* name)
  {
      // set the name
      stm::stms[id].name = name;

      // set the pointers
      stm::stms[id].begin     = NOrec2_Semantic<CM>::begin;
      stm::stms[id].commit    = NOrec2_Semantic<CM>::commit_ro;
      stm::stms[id].read      = NOrec2_Semantic<CM>::read_ro;
      stm::stms[id].write     = NOrec2_Semantic<CM>::write_ro;
      stm::stms[id].compare   = NOrec2_Semantic<CM>::compare;
      stm::stms[id].increment_decimal  = NOrec2_Semantic<CM>::increment_decimal;
      stm::stms[id].increment_floating = NOrec2_Semantic<CM>::increment_floating;
      stm::stms[id].irrevoc   = irrevoc;
      stm::stms[id].switcher  = onSwitchTo;
      stm::stms[id].privatization_safe = true;
      stm::stms[id].rollback  = NOrec2_Semantic<CM>::rollback;
  }

  template <class CM>
  bool
  NOrec2_Semantic<CM>::begin(TxThread* tx)
  {
      // Originally, NOrec required us to wait until the timestamp is odd
      // before we start.  However, we can round down if odd, in which case
      // we don't need control flow here.

      // Sample the sequence lock, if it is even decrement by 1
      tx->start_time = timestamp.val & ~(1L);
      OnBegin(tx);
      // notify the allocator
      tx->allocator.onTxBegin();

      // notify CM
      CM::onBegin(tx);

      return false;
  }

  template <class CM>
  void
  NOrec2_Semantic<CM>::commit(STM_COMMIT_SIG(tx,upper_stack_bound))
  {
      // From a valid state, the transaction increments the seqlock.  Then it
      // does writeback and increments the seqlock again

      // read-only is trivially successful at last read
      if (!tx->writes.size()) {
          CM::onCommit(tx);
          tx->tlist.reset();
          OnReadOnlyCommit(tx);
          return;
      }

      // get the lock and validate (use RingSTM obstruction-free technique)
      while (!bcasptr(&timestamp.val, tx->start_time, tx->start_time + 1))
          if ((tx->start_time = validate(tx)) == VALIDATION_FAILED)
              tx->tmabort(tx);

      tx->writes.writeback(STM_WHEN_PROTECT_STACK(upper_stack_bound));

      // Release the sequence lock, then clean up
      CFENCE;
      timestamp.val = tx->start_time + 2;
      CM::onCommit(tx);
      tx->tlist.reset();
      tx->writes.reset();
      OnReadWriteCommit(tx);
  }

  template <class CM>
  void
  NOrec2_Semantic<CM>::commit_ro(STM_COMMIT_SIG(tx,))
  {
      // Since all reads were consistent, and no writes were done, the read-only
      // NOrec transaction just resets itself and is done.
      CM::onCommit(tx);
      tx->tlist.reset();
      OnReadOnlyCommit(tx);
  }

  template <class CM>
  void
  NOrec2_Semantic<CM>::commit_rw(STM_COMMIT_SIG(tx,upper_stack_bound))
  {
      // From a valid state, the transaction increments the seqlock.  Then it does
      // writeback and increments the seqlock again

      // get the lock and validate (use RingSTM obstruction-free technique)
      while (!bcasptr(&timestamp.val, tx->start_time, tx->start_time + 1))
          if ((tx->start_time = validate(tx)) == VALIDATION_FAILED)
              tx->tmabort(tx);

      tx->writes.writeback(STM_WHEN_PROTECT_STACK(upper_stack_bound));

      // Release the sequence lock, then clean up
      CFENCE;
      timestamp.val = tx->start_time + 2;

      // notify CM
      CM::onCommit(tx);

      tx->tlist.reset();
      tx->writes.reset();

      // This switches the thread back to RO mode.
      OnReadWriteCommit(tx, read_ro, write_ro, commit_ro);
  }

  template <class CM>
  void*
  NOrec2_Semantic<CM>::read_ro(STM_READ_SIG(tx,addr,mask))
  {
#ifdef STATS
	  tx->tx_num_reads++;
#endif
      // A read is valid iff it occurs during a period where the seqlock does
      // not change and is even.  This code also polls for new changes that
      // might necessitate a validation.

      // read the location to a temp
      void* tmp = *addr;
      CFENCE;

      // if the timestamp has changed since the last read, we must validate and
      // restart this read
      while (tx->start_time != timestamp.val) {
          if ((tx->start_time = validate(tx)) == VALIDATION_FAILED)
              tx->tmabort(tx);
          tmp = *addr;
          CFENCE;
      }

      // log the address and value
      tx->tlist.insert(STM_TYPE_OPERATION_LIST_ENTRY(addr, TM_EQ, tmp, mask));
      return tmp;
  }

  template <class CM>
  void*
  NOrec2_Semantic<CM>::read_rw(STM_READ_SIG(tx,addr,mask))
  {
      // check the log for a RAW hazard, we expect to miss
	  WriteSetEntry* entry = tx->writes.retrieve(addr);
      if (entry != NULL){
    	  if(entry->type != ENTRY_WRITE){
#ifdef STATS
	  tx->tx_num_promotes++;
#endif
    		  // promote to read+write
    		  void* val = read_ro(tx, addr STM_MASK(mask & ~log.mask));
    		  entry->add(val);
    		  entry->type = ENTRY_WRITE;
    	  }
          return entry->val;
      }

      // Use the code from the read-only read barrier. This is complicated by
      // the fact that, when we are byte logging, we may have successfully read
      // some bytes from the write log (if we read them all then we wouldn't
      // make it here). In this case, we need to log the mask for the rest of the
      // bytes that we "actually" need, which is computed as bytes in mask but
      // not in log.mask. This is only correct because we know that a failed
      // find also reset the log.mask to 0 (that's part of the find interface).
      void* val = read_ro(tx, addr STM_MASK(mask & ~log.mask));
      REDO_RAW_CLEANUP(val, found, log, mask);
      return val;
  }

  template <class CM>
  bool
  NOrec2_Semantic<CM>::compare(STM_COMPARE_SIG(tx,addr,operation,val,mask))
  {
      // check the log for a RAW hazard, we expect to miss
	  WriteSetEntry* entry = tx->writes.retrieve(addr);
      if (entry != NULL){
    	  if(entry->type != ENTRY_WRITE){
#ifdef STATS
	  tx->tx_num_promotes++;
#endif
    		  // promote to read+write
    		  void* val = read_ro(tx, addr STM_MASK(mask & ~log.mask));
    		  entry->add(val);
    		  entry->type = ENTRY_WRITE;
    	  }
          return stm::operate(entry->val, operation, val);
      }
#ifdef STATS
	  tx->tx_num_compares++;
#endif

	  // A read is valid iff it occurs during a period where the seqlock does
	  // not change and is even.  This code also polls for new changes that
	  // might necessitate a validation.

	  // read the location to a temp
	  void* tmp = *addr;
	  CFENCE;

	  // if the timestamp has changed since the last read, we must validate and
	  // restart this read
	  while (tx->start_time != timestamp.val) {
		   if ((tx->start_time = validate(tx)) == VALIDATION_FAILED)
			   tx->tmabort(tx);
		   tmp = *addr;
		   CFENCE;
	  }

	  bool result = stm::operate(tmp, operation, val);
	   // log the address and value
	  tx->tlist.insert(STM_TYPE_OPERATION_LIST_ENTRY(addr, result ? operation : stm::inverse(operation), val, mask));

	  return result;
  }


  template <class CM>
  void
  NOrec2_Semantic<CM>::increment_decimal(STM_INCREMENT_I_SIG(tx,addr,val,mask))
  {
#ifdef STATS
	  tx->tx_num_increments++;
#endif
      // buffer the write, and switch to a writing context
      tx->writes.insert(WriteSetEntry(STM_DELTA_SET_ENTRY(addr, val, mask, ENTRY_DELTA_DECIMAL)));
      OnFirstWrite(tx, read_rw, write_rw, commit_rw);
  }

  template <class CM>
  void
  NOrec2_Semantic<CM>::increment_floating(STM_INCREMENT_F_SIG(tx,addr,val,mask))
  {
#ifdef STATS
	  tx->tx_num_increments++;
#endif
	  union { double d;  void*  v; } v;
	  v.d =  val;
      // buffer the write, and switch to a writing context
      tx->writes.insert(WriteSetEntry(STM_DELTA_SET_ENTRY(addr, v.v, mask, ENTRY_DELTA_FLOATING)));
      OnFirstWrite(tx, read_rw, write_rw, commit_rw);
  }

  template <class CM>
  void
  NOrec2_Semantic<CM>::write_ro(STM_WRITE_SIG(tx,addr,val,mask))
  {
#ifdef STATS
	  tx->tx_num_writes++;
#endif
      // buffer the write, and switch to a writing context
      tx->writes.insert(WriteSetEntry(STM_WRITE_SET_ENTRY(addr, val, mask)));
      OnFirstWrite(tx, read_rw, write_rw, commit_rw);
  }

  template <class CM>
  void
  NOrec2_Semantic<CM>::write_rw(STM_WRITE_SIG(tx,addr,val,mask))
  {
#ifdef STATS
	  tx->tx_num_writes++;
#endif
      // just buffer the write
      tx->writes.insert(WriteSetEntry(STM_WRITE_SET_ENTRY(addr, val, mask)));
  }

  template <class CM>
  stm::scope_t*
  NOrec2_Semantic<CM>::rollback(STM_ROLLBACK_SIG(tx, upper_stack_bound, except, len))
  {
      stm::PreRollback(tx);

      // notify CM
      CM::onAbort(tx);

      // Perform writes to the exception object if there were any... taking the
      // branch overhead without concern because we're not worried about
      // rollback overheads.
      STM_ROLLBACK(tx->writes, upper_stack_bound, except, len);

      tx->tlist.reset();
      tx->writes.reset();
      return stm::PostRollback(tx, read_ro, write_ro, commit_ro);
  }
} // (anonymous namespace)

namespace stm {

	template <>
	void initTM<S_NOrec2>() {
		NOrec2_Semantic<HyperAggressiveCM>::initialize(S_NOrec2, "S_NOrec2");
	}
}

