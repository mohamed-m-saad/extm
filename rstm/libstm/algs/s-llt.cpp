/**
 *  Copyright (C) 2011
 *  University of Rochester Department of Computer Science
 *    and
 *  Lehigh University Department of Computer Science and Engineering
 *
 * License: Modified BSD
 *          Please see the file LICENSE.RSTM for licensing information
 */

/**
 *  LLT_Semantic Implementation
 *
 *    This STM very closely resembles the GV1 variant of TL2.  That is, it uses
 *    orecs and lazy acquire.  Its clock requires everyone to increment it to
 *    commit writes, but this allows for read-set validation to be skipped at
 *    commit time.  Most importantly, there is no in-flight validation: if a
 *    timestamp is greater than when the transaction sampled the clock at begin
 *    time, the transaction aborts.
 */

#include "../profiling.hpp"
#include "algs.hpp"
#include "RedoRAWUtils.hpp"
#include "stm/OperationList.hpp"

using stm::TxThread;
using stm::timestamp;
using stm::timestamp_max;
using stm::WriteSet;
using stm::OrecList;
using stm::UNRECOVERABLE;
using stm::WriteSetEntry;
using stm::OperationList;
using stm::OperationListEntry;
using stm::orec_t;
using stm::get_orec;


/**
 *  Declare the functions that we're going to implement, so that we can avoid
 *  circular dependencies.
 */
namespace {
  struct LLT_Semantic
  {
      static TM_FASTCALL bool begin(TxThread*);
      static TM_FASTCALL void* read_ro(STM_READ_SIG(,,));
      static TM_FASTCALL void* read_rw(STM_READ_SIG(,,));
      static TM_FASTCALL void write_ro(STM_WRITE_SIG(,,,));
      static TM_FASTCALL void write_rw(STM_WRITE_SIG(,,,));
      static TM_FASTCALL bool compare(STM_COMPARE_SIG(,,,,));
      static TM_FASTCALL void increment_decimal(STM_INCREMENT_I_SIG(,,,));
      static TM_FASTCALL void increment_floating(STM_INCREMENT_F_SIG(,,,));
      static TM_FASTCALL void commit_ro(STM_COMMIT_SIG(,));
      static TM_FASTCALL void commit_rw(STM_COMMIT_SIG(,));

      static stm::scope_t* rollback(STM_ROLLBACK_SIG(,,,));
      static bool irrevoc(STM_IRREVOC_SIG(,));
      static void onSwitchTo();
      static NOINLINE void validate(TxThread*);
      static NOINLINE int validate(TxThread*, bool);
  };


#define EMPTY		100000000

#define LAZY_TIMESTAMP 		 							\
		if(tx->start_time == EMPTY){					\
			if(tx->compare_time == EMPTY){				\
  	  	  	  	  tx->start_time = timestamp.val;		\
			}else{										\
  	  	  	  	  int snapshot = validate(tx, false);   \
  	  	  	  	  if(snapshot < 0)						\
				  	  tx->tmabort(tx);					\
				  tx->start_time = snapshot;			\
  	  	  	}											\
		}

  /**
   *  LLT_Semantic begin:
   */
  bool
  LLT_Semantic::begin(TxThread* tx)
  {
      tx->allocator.onTxBegin();
      // get a start time
      tx->start_time = EMPTY;
      tx->compare_time = EMPTY;
      OnBegin(tx);
      return false;
  }

  /**
   *  LLT_Semantic commit (read-only):
   */
  void
  LLT_Semantic::commit_ro(STM_COMMIT_SIG(tx,))
  {
      // read-only, so just reset lists
      tx->r_orecs.reset();
      tx->olist.reset();
      OnReadOnlyCommit(tx);
  }

  /**
   *  LLT_Semantic commit (writing context):
   *
   *    Get all locks, validate, do writeback.  Use the counter to avoid some
   *    validations.
   */
  void
  LLT_Semantic::commit_rw(STM_COMMIT_SIG(tx,upper_stack_bound))
  {
	  uintptr_t effective_start = (tx->compare_time == EMPTY ? tx->start_time : tx->compare_time);
      // acquire locks
      foreach (WriteSet, i, tx->writes) {
          // get orec, read its version#
          orec_t* o = get_orec(i->addr);
          uintptr_t ivt = o->v.all;

          // lock all orecs, unless already locked
          if (ivt <= effective_start) {
              // abort if cannot acquire
              if (!bcasptr(&o->v.all, ivt, tx->my_lock.all))
                  tx->tmabort(tx);
              // save old version to o->p, remember that we hold the lock
              o->p = ivt;
              tx->locks.insert(o);
          }
          // else if we don't hold the lock abort
          else if (ivt != tx->my_lock.all) {
              tx->tmabort(tx);
          }
      }

      // increment the global timestamp since we have writes
      uintptr_t end_time = 1 + faiptr(&timestamp.val);

      // skip validation if nobody else committed
      if (end_time != (effective_start + 1)){
          validate(tx);
          if(validate(tx, true) < 0)
        	  tx->tmabort(tx);
      }

      // run the redo log
      tx->writes.writeback(STM_WHEN_PROTECT_STACK(upper_stack_bound));

      // release locks
      CFENCE;
      foreach (OrecList, i, tx->locks)
          (*i)->v.all = end_time;

      // clean-up
      tx->r_orecs.reset();
      tx->olist.reset();
      tx->writes.reset();
      tx->locks.reset();
      OnReadWriteCommit(tx, read_ro, write_ro, commit_ro);
  }

  /**
   *  LLT_Semantic read (read-only transaction)
   *
   *    We use "check twice" timestamps in LLT_Semantic
   */
  void*
  LLT_Semantic::read_ro(STM_READ_SIG(tx,addr,))
  {
#ifdef STATS
	  tx->tx_num_reads++;
#endif
	  LAZY_TIMESTAMP
      // get the orec addr
      orec_t* o = get_orec(addr);

      // read orec, then val, then orec
      uintptr_t ivt = o->v.all;
      CFENCE;
      void* tmp = *addr;
      CFENCE;
      uintptr_t ivt2 = o->v.all;
      // if orec never changed, and isn't too new, the read is valid
      if ((ivt <= tx->start_time) && (ivt == ivt2)) {
          // log orec, return the value
          tx->r_orecs.insert(o);
          return tmp;
      }
      // unreachable
      tx->tmabort(tx);
      return NULL;
  }

  /**
   *  LLT_Semantic read (writing transaction)
   */
  void*
  LLT_Semantic::read_rw(STM_READ_SIG(tx,addr,mask))
  {
      // check the log for a RAW hazard, we expect to miss
	  WriteSetEntry* entry = tx->writes.retrieve(addr);
      if (entry != NULL){
    	  if(entry->type != ENTRY_WRITE){
#ifdef STATS
	  tx->tx_num_promotes++;
#endif
    		  // promote to read+write
    		  void* val = read_ro(tx, addr STM_MASK(mask & ~log.mask));
    		  entry->add(val);
    		  entry->type = ENTRY_WRITE;
    	  }
          return entry->val;
      }

#ifdef STATS
	  tx->tx_num_reads++;
#endif
      LAZY_TIMESTAMP
      // get the orec addr
      orec_t* o = get_orec(addr);

      // read orec, then val, then orec
      uintptr_t ivt = o->v.all;
      CFENCE;
      void* tmp = *addr;
      CFENCE;
      uintptr_t ivt2 = o->v.all;

      // fixup is here to minimize the postvalidation orec read latency
      REDO_RAW_CLEANUP(tmp, found, log, mask);
      // if orec never changed, and isn't too new, the read is valid
      if ((ivt <= tx->start_time) && (ivt == ivt2)) {
          // log orec, return the value
          tx->r_orecs.insert(o);
          return tmp;
      }
      tx->tmabort(tx);
      // unreachable
      return NULL;
  }

  bool
  LLT_Semantic::compare(STM_COMPARE_SIG(tx,addr,operation,val,mask))
  {
      // check the log for a RAW hazard, we expect to miss
	  WriteSetEntry* entry = tx->writes.retrieve(addr);
	  if (entry != NULL){
		  if(entry->type != ENTRY_WRITE){
#ifdef STATS
	  tx->tx_num_promotes++;
#endif
			  // promote to read+write
			  void* val = read_ro(tx, addr STM_MASK(mask & ~log.mask));
			  entry->add(val);
			  entry->type = ENTRY_WRITE;
		  }
		  return stm::operate(entry->val, operation, val);
	  }
#ifdef STATS
	  tx->tx_num_compares++;
#endif

      if(tx->start_time == EMPTY && tx->compare_time == EMPTY){	// no read yet
    	  tx->compare_time = timestamp.val;
      }

      // get the orec addr
      orec_t* o = get_orec(addr);
again:
      // read orec, then val, then orec
      uintptr_t ivt = o->v.all;
      CFENCE;
      if(o->v.fields.lock){
    	  if(tx->start_time == EMPTY)	// no read yet
    		  goto again;
    	  else
    		  tx->tmabort(tx);
      }
      void* tmp = *addr;
      CFENCE;
      uintptr_t ivt2 = o->v.all;
      if(tx->start_time == EMPTY){	// no read yet
    	  if(ivt != ivt2)
    		  goto again;
      }else{
    	  if(ivt != ivt2 || ivt > tx->start_time)
    		  tx->tmabort(tx);
      }

      bool result = stm::operate(tmp, (int)operation, (void*)val);

      tx->olist.insert(STM_OPERATION_LIST_ENTRY(addr, result ? operation : stm::inverse(operation), val, mask));

      if(tx->start_time == EMPTY){	// no read yet
    	  if(ivt > tx->compare_time){
    		  int snapshot = validate(tx, false);
    		  if(snapshot < 0)
    			  tx->tmabort(tx);
    		  tx->compare_time = snapshot;
    	  }
      }
      return result;
  }

  void
  LLT_Semantic::increment_decimal(STM_INCREMENT_I_SIG(tx,addr,val,mask))
  {
#ifdef STATS
	  tx->tx_num_increments++;
#endif
      // buffer the write, and switch to a writing context
      tx->writes.insert(WriteSetEntry(STM_DELTA_SET_ENTRY(addr, val, mask, ENTRY_DELTA_DECIMAL)));
      OnFirstWrite(tx, read_rw, write_rw, commit_rw);
  }

  void
  LLT_Semantic::increment_floating(STM_INCREMENT_F_SIG(tx,addr,val,mask))
  {
#ifdef STATS
	  tx->tx_num_increments++;
#endif
	  union { double d;  void*  v; } v;
	  v.d =  val;
      // buffer the write, and switch to a writing context
      tx->writes.insert(WriteSetEntry(STM_DELTA_SET_ENTRY(addr, v.v, mask, ENTRY_DELTA_FLOATING)));
      OnFirstWrite(tx, read_rw, write_rw, commit_rw);
  }
  /**
   *  LLT_Semantic write (read-only context)
   */
  void
  LLT_Semantic::write_ro(STM_WRITE_SIG(tx,addr,val,mask))
  {
#ifdef STATS
	  tx->tx_num_writes++;
#endif
      // add to redo log
      tx->writes.insert(WriteSetEntry(STM_WRITE_SET_ENTRY(addr, val, mask)));
      OnFirstWrite(tx, read_rw, write_rw, commit_rw);
  }

  /**
   *  LLT_Semantic write (writing context)
   */
  void
  LLT_Semantic::write_rw(STM_WRITE_SIG(tx,addr,val,mask))
  {
#ifdef STATS
	  tx->tx_num_writes++;
#endif
      // add to redo log
      tx->writes.insert(WriteSetEntry(STM_WRITE_SET_ENTRY(addr, val, mask)));
  }

  /**
   *  LLT_Semantic unwinder:
   */
  stm::scope_t*
  LLT_Semantic::rollback(STM_ROLLBACK_SIG(tx, upper_stack_bound, except, len))
  {
      PreRollback(tx);

      // Perform writes to the exception object if there were any... taking the
      // branch overhead without concern because we're not worried about
      // rollback overheads.
      STM_ROLLBACK(tx->writes, upper_stack_bound, except, len);

      // release the locks and restore version numbers
      foreach (OrecList, i, tx->locks)
          (*i)->v.all = (*i)->p;

      // undo memory operations, reset lists
      tx->r_orecs.reset();
      tx->olist.reset();
      tx->writes.reset();
      tx->locks.reset();
      return PostRollback(tx, read_ro, write_ro, commit_ro);
  }

  /**
   *  LLT_Semantic in-flight irrevocability:
   */
  bool
  LLT_Semantic::irrevoc(STM_IRREVOC_SIG(,))
  {
      return false;
  }

  /**
   *  LLT_Semantic validation
   */
  void
  LLT_Semantic::validate(TxThread* tx)
  {
      // validate
      foreach (OrecList, i, tx->r_orecs) {
          uintptr_t ivt = (*i)->v.all;
          // if unlocked and newer than start time, abort
          if ((ivt > tx->start_time) && (ivt != tx->my_lock.all))
              tx->tmabort(tx);
      }
  }

  int
  LLT_Semantic::validate(TxThread* tx, bool committing)
  {
#ifdef STATS
	  tx->tx_num_validates++;
#endif
     // validate compare-set
	 uintptr_t effective_start = (tx->compare_time == EMPTY ? tx->start_time : tx->compare_time);
	 int backoff = committing ? 10000 : 10000000;
	 while (true) {
		  // read the lock until it is even
		  uintptr_t s = timestamp.val;
		  // check the compare set
		  CFENCE;
		  // don't branch in the loop---consider it backoff if we fail
		  // validation early
		  foreach (OperationList, i, tx->olist){
			  // get the orec addr
			  orec_t* o = get_orec(i->addr);
			  if(o->v.all < effective_start)
				  continue;
			  if(o->v.fields.lock && o->v.all != tx->my_lock.all){
				  for(int i=0; i<backoff && o->v.fields.lock; i++);	// back-off
				  if((o->v.fields.lock && o->v.all != tx->my_lock.all)){
//					  std::cout << tx->id << " timeout invalid\n";
					  return -1;	// still locked after back-off
				  }
			  }
			  if(!i->isValid()){
//				  std::cout << tx->id << " invalid\n";
				  return -1;
			  }
		  }

		  // restart if timestamp changed during read set iteration
		  CFENCE;
		  if (timestamp.val == s){
//			  std::cout << tx->id << " valid till " << s << "\n";
			  return s;
		  }
	  }
	 return -1;
  }

  /**
   *  Switch to LLT_Semantic:
   *
   *    The timestamp must be >= the maximum value of any orec.  Some algs use
   *    timestamp as a zero-one mutex.  If they do, then they back up the
   *    timestamp first, in timestamp_max.
   */
  void
  LLT_Semantic::onSwitchTo()
  {
      timestamp.val = MAXIMUM(timestamp.val, timestamp_max.val);
  }
}

namespace stm {
  /**
   *  LLT_Semantic initialization
   */
  template<>
  void initTM<S_LLT>()
  {
      // set the name
      stms[S_LLT].name      = "S_LLT";

      // set the pointers
      stms[S_LLT].begin     = ::LLT_Semantic::begin;
      stms[S_LLT].commit    = ::LLT_Semantic::commit_ro;
      stms[S_LLT].read      = ::LLT_Semantic::read_ro;
      stms[S_LLT].write     = ::LLT_Semantic::write_ro;
      stms[S_LLT].compare   = LLT_Semantic::compare;
      stms[S_LLT].increment_decimal  = LLT_Semantic::increment_decimal;
      stms[S_LLT].increment_floating = LLT_Semantic::increment_floating;
      stms[S_LLT].rollback  = ::LLT_Semantic::rollback;
      stms[S_LLT].irrevoc   = ::LLT_Semantic::irrevoc;
      stms[S_LLT].switcher  = ::LLT_Semantic::onSwitchTo;
      stms[S_LLT].privatization_safe = false;
  }
}
