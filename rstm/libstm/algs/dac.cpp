/**
 *  Copyright (C) 2015
 *  Virginia Tech
 *
 * License: Modified BSD
 *          Please see the file LICENSE.RSTM for licensing information
 */

/**
 *  Direct Access STM Implementation
 *
 *    This STM is dummy and not thread-safe, just use it for
 *    profiling and debugging
 */

#include "../cm.hpp"
#include "algs.hpp"
#include "RedoRAWUtils.hpp"

// Don't just import everything from stm. This helps us find bugs.
using stm::TxThread;
using stm::timestamp;
using stm::WriteSetEntry;
using stm::ValueList;
using stm::ValueListEntry;


namespace {

  struct DAC
  {
      static TM_FASTCALL bool begin(TxThread*);
      static TM_FASTCALL void commit(STM_COMMIT_SIG(,));
      static TM_FASTCALL void commit_ro(STM_COMMIT_SIG(,));
      static TM_FASTCALL void commit_rw(STM_COMMIT_SIG(,));
      static TM_FASTCALL void* read_ro(STM_READ_SIG(,,));
      static TM_FASTCALL void* read_rw(STM_READ_SIG(,,));
      static TM_FASTCALL void write_ro(STM_WRITE_SIG(,,,));
      static TM_FASTCALL void write_rw(STM_WRITE_SIG(,,,));
      static TM_FASTCALL bool compare(STM_COMPARE_SIG(,,,,));
      static TM_FASTCALL void increment_decimal(STM_INCREMENT_I_SIG(,,,));
      static TM_FASTCALL void increment_floating(STM_INCREMENT_F_SIG(,,,));
      static stm::scope_t* rollback(STM_ROLLBACK_SIG(,,,));
      static void initialize(int id, const char* name);
      static bool irrevoc(STM_IRREVOC_SIG(,));
      static void onSwitchTo();
  };

  bool
  DAC::irrevoc(STM_IRREVOC_SIG(tx,upper_stack_bound))
  {
      return true;
  }

  void
  DAC::onSwitchTo() {
  }


  void
  DAC::initialize(int id, const char* name)
  {
  }


  bool
  DAC::begin(TxThread* tx)
  {
      return false;
  }


  void
  DAC::commit(STM_COMMIT_SIG(tx,upper_stack_bound))
  {
  }


  void
  DAC::commit_ro(STM_COMMIT_SIG(tx,))
  {
  }


  void
  DAC::commit_rw(STM_COMMIT_SIG(tx,upper_stack_bound))
  {
  }


  void*
  DAC::read_ro(STM_READ_SIG(tx,addr,mask))
  {
      return *addr;
  }


  void*
  DAC::read_rw(STM_READ_SIG(tx,addr,mask))
  {
	  return *addr;
  }


  bool
  DAC::compare(STM_COMPARE_SIG(tx,addr,operation,val,mask))
  {
	  void* v = read_rw(tx,addr);
	  return stm::operate((void*)v, (int)operation, (void*)val);
  }


  void
  DAC::increment_decimal(STM_INCREMENT_I_SIG(tx,addr,val,mask))
  {
	  void* v = read_rw(tx,addr);
	  write_ro(tx, addr, (void*)((long)v + (long)val));
  }


  void
  DAC::increment_floating(STM_INCREMENT_F_SIG(tx,addr,val,mask))
  {
	  union { double d;  void*  v; } v;
	  v.v = read_rw(tx,addr);
	  v.d +=  (double)val;
	  write_ro(tx, addr, v.v);
  }


  void
  DAC::write_ro(STM_WRITE_SIG(tx,addr,val,mask))
  {
	  *addr = val;
  }


  void
  DAC::write_rw(STM_WRITE_SIG(tx,addr,val,mask))
  {
	  *addr = val;
  }


  stm::scope_t*
  DAC::rollback(STM_ROLLBACK_SIG(tx, upper_stack_bound, except, len))
  {
      return stm::PostRollback(tx, read_ro, write_ro, commit_ro);
  }
} // (anonymous namespace)


namespace stm {
  /**
   *  DAC initialization
   */
  template<>
  void initTM<DIRECT>()
  {
      // set the name
      stms[DIRECT].name      = "DIRECT";

      // set the pointers
      stms[DIRECT].begin     = ::DAC::begin;
      stms[DIRECT].commit    = ::DAC::commit_ro;
      stms[DIRECT].read      = ::DAC::read_ro;
      stms[DIRECT].write     = ::DAC::write_ro;
      stms[DIRECT].compare   = DAC::compare;
      stms[DIRECT].increment_decimal  = DAC::increment_decimal;
      stms[DIRECT].increment_floating = DAC::increment_floating;
      stms[DIRECT].rollback  = ::DAC::rollback;
      stms[DIRECT].irrevoc   = ::DAC::irrevoc;
      stms[DIRECT].switcher  = ::DAC::onSwitchTo;
      stms[DIRECT].privatization_safe = false;
  }
}


#undef FOREACH_NOREC
#undef INIT_NOREC
