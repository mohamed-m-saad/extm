/**
 *  Copyright (C) 2015
 *  Virginia Tech
 *
 * License: Modified BSD
 *          Please see the file LICENSE.RSTM for licensing information
 */

#include <stm/config.h>

#if defined(STM_CPU_SPARC)
#include <sys/types.h>
#endif

#include <stdint.h>
#include <iostream>
#include <api/api.hpp>
#include <alt-license/rand_r_32.h>
#include "bmconfig.hpp"

/**
 *  We provide the option to build the entire benchmark in a single
 *  source. The bmconfig.hpp include defines all of the important functions
 *  that are implemented in this file, and bmharness.cpp defines the
 *  execution infrastructure.
 */
#ifdef SINGLE_SOURCE_BUILD
#include "bmharness.cpp"
#endif

/**
 *  Step 1:
 *    Include the configuration code for the harness, and the API code.
 */

/**
 *  Step 2:
 *    Declare the data type that will be stress tested via this benchmark.
 *    Also provide any functions that will be needed to manipulate the data
 *    type.  Take care to avoid unnecessary indirection.
 *
 *  NB: For the simple counter, we don't need to have an abstract data type
 */
#define ADDRESS_RANGE	10000
#define TYPE	long

struct entry{
	TYPE data;
	TYPE address;
	TYPE hits;
};

struct entry **cache;

/**
 *  Step 3:
 *    Declare an instance of the data type, and provide init, test, and verify
 *    functions
 */

/*** Initialize the counter */
void
bench_init()
{
	cache = (struct entry **)malloc(CFG.elements * sizeof(struct entry *));
	for (unsigned int index=0; index<CFG.elements; index++){
		cache[index] = (struct entry *)malloc(CFG.sets * sizeof(struct entry));
		for(int i=0; i<CFG.sets; i++){
			cache[index][i].data = -1;
			cache[index][i].address = -1;
			cache[index][i].hits = 0;
		}
	}
}

/*** Run a bunch of increment transactions */
void
bench_test(uintptr_t, uint32_t* seed)
{
	VOLATILE uint32_t local_seed = *seed;
    TM_BEGIN(atomic) {
    	for(unsigned int o=0; o<CFG.ops; o++){
    		unsigned int op = rand_r_32(&local_seed) % 100;
			long address = rand_r_32(&local_seed) % ADDRESS_RANGE;
			int index = address%CFG.elements;
    		if(op < CFG.lookpct){
    			// lookup cache (get)
				for(int i=0; i<CFG.sets; i++){
					if(TM_COMPARE(cache[index][i].address, TM_EQ, address)){
						TM_INCREMENT(cache[index][i].hits, 1l);	// increase the frequency counter
						TM_READ(cache[index][i].data);	// read the cache
						break;
					}
				}
    		}else{
    			long data = rand_r_32(&local_seed);
    			// update cache (set)
				long min = 10000000;
				int replacment = -1;
				bool found = false;
				for(int i=0; i<CFG.sets; i++){
					if(TM_COMPARE(cache[index][i].address, TM_EQ, address)){
						TM_WRITE(cache[index][i].data, data);
						found = true;
						break;
					}
					if(TM_COMPARE(cache[index][i].hits, TM_LT, min)){
						min = TM_READ(cache[index][i].hits);
						replacment = i;
					}
				}
				if(!found){
					TM_WRITE(cache[index][replacment].address, address);
					TM_WRITE(cache[index][replacment].data, data);
					TM_WRITE(cache[index][replacment].hits, 1l);
				}
    		}
    	}
    } TM_END;
    *seed = local_seed;
}

/*** Ensure the final state of the benchmark satisfies all invariants */
bool
bench_verify()
{
	for(unsigned int index=0; index<CFG.elements; index++){
		for(int i=0; i<CFG.sets; i++){
			std::cout << cache[index][i].address << "[" << cache[index][i].data << "][" << cache[index][i].hits << "] \t";
		}
		std::cout << "\n";
	}
	return true;
}

/**
 *  Step 4:
 *    Include the code that has the main() function, and the code for creating
 *    threads and calling the three above-named functions.  Don't forget to
 *    provide an arg reparser.
 */

/*** no reparsing needed */
void
bench_reparse() {
    CFG.bmname = "LRU";
}
