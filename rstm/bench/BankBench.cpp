/**
 *  Copyright (C) 2015
 *  Virginia Tech
 *
 * License: Modified BSD
 *          Please see the file LICENSE.RSTM for licensing information
 */

#include <stm/config.h>

#if defined(STM_CPU_SPARC)
#include <sys/types.h>
#endif

#include <stdint.h>
#include <iostream>
#include <api/api.hpp>
#include <alt-license/rand_r_32.h>
#include "bmconfig.hpp"

/**
 *  We provide the option to build the entire benchmark in a single
 *  source. The bmconfig.hpp include defines all of the important functions
 *  that are implemented in this file, and bmharness.cpp defines the
 *  execution infrastructure.
 */
#ifdef SINGLE_SOURCE_BUILD
#include "bmharness.cpp"
#endif

/**
 *  Step 1:
 *    Include the configuration code for the harness, and the API code.
 */

/**
 *  Step 2:
 *    Declare the data type that will be stress tested via this benchmark.
 *    Also provide any functions that will be needed to manipulate the data
 *    type.  Take care to avoid unnecessary indirection.
 *
 *  NB: For the simple counter, we don't need to have an abstract data type
 */
#define TYPE		long
#define BALANCE		10000

struct account{
	TYPE balance;
};

struct account *accounts;

/**
 *  Step 3:
 *    Declare an instance of the data type, and provide init, test, and verify
 *    functions
 */

/*** Initialize the counter */
void
bench_init()
{
	accounts = new struct account[CFG.elements];
	for(unsigned int i=0; i<CFG.elements; i++)
		accounts[i].balance = BALANCE;
}

/*** Run a bunch of increment transactions */
void
bench_test(uintptr_t, uint32_t* seed)
{
	VOLATILE uint32_t local_seed = *seed;
    TM_BEGIN(atomic) {
    	for(unsigned int i=0; i<CFG.ops; i++){
			int from = rand_r_32(&local_seed) % CFG.elements;
			int to = rand_r_32(&local_seed) % CFG.elements;
			long amount = rand_r_32(&local_seed) % BALANCE;
//	    	std::cout << from << " ----(" << amount << ")---->> " << to << "\n";
			if(TM_COMPARE(accounts[from].balance, TM_GTE, amount)){
				TM_INCREMENT(accounts[from].balance, -1 * amount);
				TM_INCREMENT(accounts[to].balance, amount);
			}
    	}
    } TM_END;
//    bench_verify();
    *seed = local_seed;
}

/*** Ensure the final state of the benchmark satisfies all invariants */
bool
bench_verify()
{
	TYPE total = 0;
	for(unsigned int i=0; i<CFG.elements; i++){
		total += accounts[i].balance;
		std::cout << "Account " << i << "\t(" << accounts[i].balance << "$)\n";
	}
	std::cout << "Total \t" << total <<"$\n";
    return total == CFG.elements * BALANCE;
}

/**
 *  Step 4:
 *    Include the code that has the main() function, and the code for creating
 *    threads and calling the three above-named functions.  Don't forget to
 *    provide an arg reparser.
 */

/*** no reparsing needed */
void
bench_reparse() {
    CFG.bmname = "Bank";
}
