/* =============================================================================
 *
 * normal.c
 * -- Implementation of normal k-means clustering algorithm
 *
 * =============================================================================
 *
 * Author:
 *
 * Wei-keng Liao
 * ECE Department, Northwestern University
 * email: wkliao@ece.northwestern.edu
 *
 *
 * Edited by:
 *
 * Jay Pisharath
 * Northwestern University.
 *
 * Chi Cao Minh
 * Stanford University
 *
 * =============================================================================
 *
 * For the license of bayes/sort.h and bayes/sort.c, please see the header
 * of the files.
 * 
 * ------------------------------------------------------------------------
 * 
 * For the license of kmeans, please see kmeans/LICENSE.kmeans
 * 
 * ------------------------------------------------------------------------
 * 
 * For the license of ssca2, please see ssca2/COPYRIGHT
 * 
 * ------------------------------------------------------------------------
 * 
 * For the license of lib/mt19937ar.c and lib/mt19937ar.h, please see the
 * header of the files.
 * 
 * ------------------------------------------------------------------------
 * 
 * For the license of lib/rbtree.h and lib/rbtree.c, please see
 * lib/LEGALNOTICE.rbtree and lib/LICENSE.rbtree
 * 
 * ------------------------------------------------------------------------
 * 
 * Unless otherwise noted, the following license applies to STAMP files:
 * 
 * Copyright (c) 2007, Stanford University
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 * 
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in
 *       the documentation and/or other materials provided with the
 *       distribution.
 * 
 *     * Neither the name of Stanford University nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY STANFORD UNIVERSITY ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL STANFORD UNIVERSITY BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * longERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 *
 * =============================================================================
 */


#include "normal.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include <math.h>
#include "../lib/random.h"
#include "../lib/thread.h"
#include "../lib/timer.h"
#include "../lib/tm.h"
#include "common.h"
#include "util.h"

double global_time = 0.0;

typedef struct args {
    double** feature;
    long     nfeatures;
    long     npolongs;
    long     nclusters;
    long*    membership;
    double** clusters;
    long**   new_centers_len;
    double** new_centers;
} args_t;

double global_delta;
long global_i; /* index longo task queue */

#define CHUNK 3l


/* =============================================================================
 * work
 * =============================================================================
 */
static void
work (void* argPtr)
{
    TM_THREAD_ENTER();

    args_t* args = (args_t*)argPtr;
    double** feature         = args->feature;
    long     nfeatures       = args->nfeatures;
    long     npolongs         = args->npolongs;
    long     nclusters       = args->nclusters;
    long*    membership      = args->membership;
    double** clusters        = args->clusters;
    long**   new_centers_len = args->new_centers_len;
    double** new_centers     = args->new_centers;
    double delta = 0.0;
    long index;
    long i;
    long j;
    long start;
    long stop;
    long myId;

    myId = thread_getId();

    start = myId * CHUNK;

    while (start < npolongs) {
        stop = (((start + CHUNK) < npolongs) ? (start + CHUNK) : npolongs);
        for (i = start; i < stop; i++) {

            index = common_findNearestPoint(feature[i],
                                            nfeatures,
                                            clusters,
                                            nclusters);
            /*
             * If membership changes, increase delta by 1.
             * membership[i] cannot be changed by other threads
             */
            if (membership[i] != index) {
                delta += 1.0;
            }

            /* Assign the membership to object i */
            /* membership[i] can't be changed by other thread */
            membership[i] = index;

            /* Update new cluster centers : sum of objects located within */
            TM_BEGIN();
            TM_SHARED_INCREMENT(*new_centers_len[index], 1l);
            for (j = 0; j < nfeatures; j++) {
                TM_SHARED_INCREMENT(new_centers[index][j], feature[i][j]);
            }
            TM_END();
        }

        /* Update task queue */
        if (start + CHUNK < npolongs) {
            TM_BEGIN();
            start = (long)TM_SHARED_READ_L(global_i);
            TM_SHARED_INCREMENT(global_i, CHUNK);
            TM_END();
        } else {
            break;
        }
    }

    TM_BEGIN();
    TM_SHARED_INCREMENT(global_delta, delta);
    TM_END();

    TM_THREAD_EXIT();
}


/* =============================================================================
 * normal_exec
 * =============================================================================
 */
double**
normal_exec (long       nthreads,
             double**   feature,    /* in: [npolongs][nfeatures] */
             long       nfeatures,
             long       npolongs,
             long       nclusters,
             double     threshold,
             long*      membership,
             random_t* randomPtr) /* out: [npolongs] */
{
    long i;
    long j;
    long loop = 0;
    long** new_centers_len; /* [nclusters]: no. of polongs in each cluster */
    double delta;
    double** clusters;      /* out: [nclusters][nfeatures] */
    double** new_centers;   /* [nclusters][nfeatures] */
    void* alloc_memory = NULL;
    args_t args;
    TIMER_T start;
    TIMER_T stop;

    /* Allocate space for returning variable clusters[] */
    clusters = (double**)malloc(nclusters * sizeof(double*));
    assert(clusters);
    clusters[0] = (double*)malloc(nclusters * nfeatures * sizeof(double));
    assert(clusters[0]);
    for (i = 1; i < nclusters; i++) {
        clusters[i] = clusters[i-1] + nfeatures;
    }

    /* Randomly pick cluster centers */
    for (i = 0; i < nclusters; i++) {
        long n = (long)(random_generate(randomPtr) % npolongs);
        for (j = 0; j < nfeatures; j++) {
            clusters[i][j] = feature[n][j];
        }
    }

    for (i = 0; i < npolongs; i++) {
        membership[i] = -1;
    }

    /*
     * Need to initialize new_centers_len and new_centers[0] to all 0.
     * Allocate clusters on different cache lines to reduce false sharing.
     */
    {
        long cluster_size = sizeof(long) + sizeof(double) * nfeatures;
        const long cacheLineSize = 32;
        cluster_size += (cacheLineSize-1) - ((cluster_size-1) % cacheLineSize);
        alloc_memory = calloc(nclusters, cluster_size);
        new_centers_len = (long**) malloc(nclusters * sizeof(long*));
        new_centers = (double**) malloc(nclusters * sizeof(double*));
        assert(alloc_memory && new_centers && new_centers_len);
        for (i = 0; i < nclusters; i++) {
            new_centers_len[i] = (long*)((char*)alloc_memory + cluster_size * i);
            new_centers[i] = (double*)((char*)alloc_memory + cluster_size * i + sizeof(long));
        }
    }

    TIMER_READ(start);

    GOTO_SIM();

    do {
        delta = 0.0;

        args.feature         = feature;
        args.nfeatures       = nfeatures;
        args.npolongs         = npolongs;
        args.nclusters       = nclusters;
        args.membership      = membership;
        args.clusters        = clusters;
        args.new_centers_len = new_centers_len;
        args.new_centers     = new_centers;

        global_i = nthreads * CHUNK;
        global_delta = delta;

#ifdef OTM
#pragma omp parallel
        {
            work(&args);
        }
#else
        thread_start(work, &args);
#endif

        delta = global_delta;

        /* Replace old cluster centers with new_centers */
        for (i = 0; i < nclusters; i++) {
            for (j = 0; j < nfeatures; j++) {
                if (new_centers_len[i] > 0) {
                    clusters[i][j] = new_centers[i][j] / *new_centers_len[i];
                }
                new_centers[i][j] = 0.0;   /* set back to 0 */
            }
            *new_centers_len[i] = 0;   /* set back to 0 */
        }

        delta /= npolongs;

    } while ((delta > threshold) && (loop++ < 500));

    GOTO_REAL();

    TIMER_READ(stop);
    global_time += TIMER_DIFF_SECONDS(start, stop);

    free(alloc_memory);
    free(new_centers);
    free(new_centers_len);

    return clusters;
}


/* =============================================================================
 *
 * End of normal.c
 *
 * =============================================================================
 */
